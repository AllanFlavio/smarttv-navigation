import React from "react";
import "./menu.scss";

const MenuItem = ({ title, isFocused }) => (
  <div className="menu-item" style={{...(isFocused ? styles.menuFocused : null) }}
  >
    <span style={styles.label}>{title}</span>
  </div>
);

const styles = {
  menuFocused: {
    backgroundColor: "white",
    color: "black"
  }
};

export default MenuItem;
