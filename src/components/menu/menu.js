import React from "react";
import { menuPaths } from "../../model/Node";
import MenuItem from "./MenuItem";
import './menu.scss'

const Menu = ({ currentNode }) => {
  return (
    <div className="main-menu">
      {menuPaths.map((menu, index) => {
        return (
          <MenuItem
            key={index}
            title={menu.id}
            isFocused={menu.id === currentNode.id}
          />
        );
      })}
    </div>
  );
};

export default Menu;
