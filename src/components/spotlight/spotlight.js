import React from 'react';
import { spotlightPaths }from "../../model/Node";
import ButtonItem from "./ButtonItem";
import './spotlight.scss';


const Spotlight = ({ currentNode }) => {

  return (
    <div className="main-spotlight">
      <div className="main-spotlight-content">
        {spotlightPaths.map((button, index) => {
          return (
            <ButtonItem 
              key={index}
              title={button.id}
              isFocused={button.id === currentNode.id}
            />
            )
          })
        }
      </div>
    </div>
  );
}


export default Spotlight;