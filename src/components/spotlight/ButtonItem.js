import React from "react";
import "./spotlight.scss"

const ButtonItem = ({ title, isFocused }) => (
  <div className="button" style={{...(isFocused ? styles.buttonFocused : null)}}>
    <span style={styles.label}>{title}</span>
  </div>
);

const styles = {
  buttonFocused: {
    backgroundColor: "white",
    color: "black"
  }
};

export default ButtonItem;