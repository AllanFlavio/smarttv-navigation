import React from 'react';
import GalleryItem from './GalleryItem'
import {galleyRowsPaths, galleryPaths} from "../../model/Node";
import './gallery.scss'

const GalleryRow = ({ isFocused, currentNode, galleryRow }) => {
  const styles = {
    focus: {
      backgroundColor: "black"
    }
  }
  return (
    <div className="gallery-row" style={{...(isFocused ? styles.focus : null) }}>
      {
        galleryPaths.map((gallery, index) => {
          if (galleryRow.id && gallery[galleryRow.id]) {
            return gallery[galleryRow.id].map((item, index) => {
              return <GalleryItem
                key={index}
                title={item.id}
                isFocused={item.id === currentNode.id}
              />;
            })
          }
        })
      }
    </div>
  );
}


const Gallery = ({ currentNode }) => {
  
  return (
    <div className="main-gallery">
      <div className="gallery-content">
        {galleyRowsPaths.map((gallery_row, index) => {
          return (
            <GalleryRow
              key={index}
              title={gallery_row.id}
              currentNode={currentNode}
              galleryRow={gallery_row}
              isFocused={gallery_row.id === currentNode.id}
            />
          );
        })}
      </div>
    </div>
  );
}

export default Gallery;
