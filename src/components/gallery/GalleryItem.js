import React from 'react';
import './gallery.scss'

const GalleryItem = ({ title, isFocused }) => {
  const styles = {
    focus: {
      backgroundColor: "black",
      color: "white"
    }
  };

  return (
    <div className="gallery-item" style={{...(isFocused ? styles.focus : null) }}>
      <span>{title}</span>
    </div>
  );
}

export default GalleryItem;