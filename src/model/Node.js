export class Node {
  topTarget = null;
  rightTarget = null;
  bottomTarget = null;
  leftTarget = null;
  constructor(id, topTarget, rightTarget, bottomTarget, leftTarget) {
    this.id = id;
    this.topTarget = topTarget;
    this.rightTarget = rightTarget;
    this.bottomTarget = bottomTarget;
    this.leftTarget = leftTarget;
  }
}

export const menuPaths = [
  new Node("menu1", null, "spotlight1", "menu2", null),
  new Node("menu2", "menu1", "spotlight1", "menu3", null),
  new Node("menu3", "menu2", "spotlight1", "menu4", null),
  new Node("menu4", "menu3", "spotlight1", "menu5", null),
  new Node("menu5", "menu4", "spotlight1", null, null)
];

export const spotlightPaths = [
  new Node("spotlight1", null, "spotlight2", "galleryRow1", "menu1"),
  new Node("spotlight2", null, null, "galleryRow1", "spotlight1")
];

export const galleyRowsPaths = [
  new Node("galleryRow1", "spotlight1", "gallery1", "galleryRow2", "menu1"),
  new Node("galleryRow2", "galleryRow1", "gallery1", "galleryRow3", "menu1")
];

const galPath1 = [
  new Node("gallery1", "spotlight1", "gallery2", "gallery8", "menu1"),
  new Node("gallery2", "spotlight1", "gallery3", "gallery9", "gallery1"),
  new Node("gallery3", "spotlight1", "gallery4", "gallery10", "gallery2"),
  new Node("gallery4", "spotlight1", "gallery5", "gallery11", "gallery3"),
  new Node("gallery5", "spotlight1", "gallery6", "gallery12", "gallery4"),
  new Node("gallery6", "spotlight1", "gallery7", "gallery13", "gallery5"),
  new Node("gallery7", "spotlight1", null, "gallery14", "gallery6")
];

const galPath2 = [
  new Node("gallery8", "gallery1", "gallery9", null, "menu1"),
  new Node("gallery9", "gallery2", "gallery10", null, "gallery8"),
  new Node("gallery10", "gallery3", "gallery11", null, "gallery9"),
  new Node("gallery11", "gallery4", "gallery12", null, "gallery10"),
  new Node("gallery12", "gallery5", "gallery13", null, "gallery11"),
  new Node("gallery13", "gallery6", "gallery14", null, "gallery12"),
  new Node("gallery14", "gallery7", null, null, "gallery13")
];

export const galleryPaths = [
  {
    galleryRow1: galPath1
  },
  {
    galleryRow2: galPath2
  }
];

export const allGaleryPaths = [...galPath1, ...galPath2];
