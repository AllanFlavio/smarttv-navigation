import React, { Component } from "react";
import Menu from "../components/menu/menu";
import Spotlight from "../components/spotlight/spotlight";
import Gallery from "../components/gallery/gallery";
import {
  menuPaths,
  spotlightPaths,
  galleyRowsPaths,
  allGaleryPaths
} from "../model/Node";
import "./App.scss";

const allPaths = [
  ...menuPaths,
  ...spotlightPaths,
  ...galleyRowsPaths,
  ...allGaleryPaths
];

class App extends Component {
  state = {
    currentNode: allPaths[0]
  };

  onKeydownEvent = keydown => {
    const targetNode = this.getTargetNodeFromDirection(keydown);
    if (targetNode) {
      this.setState({ currentNode: targetNode });
    }
  };

  componentDidMount() {
    window.addEventListener("keydown", ({ key }) => this.onKeydownEvent(key));
  }

  getTargetNodeFromDirection = direction => {
    const { currentNode } = this.state;
    switch (direction) {
      case "ArrowUp":
        if (!currentNode.topTarget) return null;
        return this.getNodeWithId(currentNode.topTarget);

      case "ArrowRight":
        if (!currentNode.rightTarget) return null;
        return this.getNodeWithId(currentNode.rightTarget);

      case "ArrowDown":
        if (!currentNode.bottomTarget) return null;
        return this.getNodeWithId(currentNode.bottomTarget);

      case "ArrowLeft":
        if (!currentNode.leftTarget) return null;
        return this.getNodeWithId(currentNode.leftTarget);
      default:
        return null;
    }
  };

  getNodeWithId = id => {
    return allPaths.find(item => item.id === id);
  };

  render() {
    return (
      <div className="main-page">
        <div className="main-page-menu">
          <Menu currentNode={this.state.currentNode} />
        </div>
        <div className="main-page-content">
          <Spotlight currentNode={this.state.currentNode} />
          <Gallery currentNode={this.state.currentNode} />
        </div>
      </div>
    );
  }
}

export default App;
